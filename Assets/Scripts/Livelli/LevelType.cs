using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public enum Ingredients
{
    Bread,
    Tomato,
    Cheese,
    Salad
}


[Serializable]
public class DefinitionMaps
{
    public Vector2 position;
    public Ingredients Ingredients;
    [HideInInspector] public GameObject ObjIngredients;
}


[CreateAssetMenu(fileName = "ScriptableObject", menuName = " ScriptableObject/CreateLevel")]
public class LevelType : ScriptableObject
{
    [Header("VARIABLE\n")]
    [SerializeField] private List<DefinitionMaps> maps = new List<DefinitionMaps>();
    [Header("Prefabs\n")]
    [SerializeField] private List<GameObject> prefabs = new List<GameObject>();

    public List<DefinitionMaps> _maps { get => maps; }

    private void OnEnable()
    {
        for (int i = 0; i < maps.Count; i++)
        {
            switch (maps[i].Ingredients)
            {
                case Ingredients.Bread:
                    maps[i].ObjIngredients = prefabs[0];
                    break;
                case Ingredients.Tomato:
                    maps[i].ObjIngredients = prefabs[1];
                    break;
                case Ingredients.Cheese:
                    maps[i].ObjIngredients = prefabs[2];
                    break;
                case Ingredients.Salad:
                    maps[i].ObjIngredients = prefabs[3];
                    break;
                default:
                    break;
            }
        }
    }
}
