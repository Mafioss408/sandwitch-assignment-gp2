using System.Collections.Generic;
using UnityEngine;

public class MapGeneration : MonoBehaviour
{
    #region Matrix
    private const int k_raw = 4;
    private const int k_column = 4;
    private GameObject[,] prefabsMap = new GameObject[k_raw, k_column];

    private List<GameObject> allIngredients = new List<GameObject>();
    #endregion

    private GameManager GM;

    [Header("Levels")]
    [SerializeField] private List<LevelType> levels = new List<LevelType>();

    #region GET/SET
    public GameObject[,] _prefabsMap { get => prefabsMap; set => prefabsMap = value; }
    public List<GameObject> _allIngredients => allIngredients;
    public List<LevelType> _levels => levels;
    #endregion

    private void Start()
    {
        GM = GameManager.Instance;
        transform.position = new Vector3((int)transform.position.x, 0, (int)transform.position.z);
    }
    private GameObject InstantiateMap(int raw, int column, LevelType level)
    {
        foreach (var item in level._maps)
        {
            if (raw == item.position.x && column == item.position.y)
            {
                var go = Instantiate(item.ObjIngredients, new Vector3(transform.position.x + raw, 0, transform.position.z + column), Quaternion.identity);
                go.transform.parent = transform;
                allIngredients.Add(go);
                GM._objInGame++;
                return go;
            }
        }
        return null;
    }
    public void CreateLevel()
    {
        if (allIngredients.Count > 0)
        {
            for (int i = 0; i < allIngredients.Count; i++)
            {
                Destroy(allIngredients[i].gameObject);
            }

            GM._objInGame = 0;
            GM._currentMove = 0;
            allIngredients.Clear();
            GM._games.Clear();
        }

        for (int i = 0; i < prefabsMap.GetLength(0); i++)
        {
            for (int j = 0; j < prefabsMap.GetLength(1); j++)
            {
                prefabsMap[i, j] = InstantiateMap(i, j, levels[GM._currentLvl]);
            }
        }
    }

#if UNITY_EDITOR

    [ContextMenu("Print")]
    private void PrintMap()
    {
        for (int i = 0; i < prefabsMap.GetLength(0); i++)
        {
            for (int j = 0; j < prefabsMap.GetLength(1); j++)
            {
                Debug.Log(prefabsMap[i, j]);
            }
        }
    }
#endif
}
