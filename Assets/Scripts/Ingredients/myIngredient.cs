using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(BoxCollider))]
[RequireComponent(typeof(Rigidbody))]
public abstract class myIngredient : MonoBehaviour
{
    private List<GameObject> m_otherIngredients = new List<GameObject>();
    private float currentHeight = 0f;

    public List<GameObject> _otherIngredients { get => m_otherIngredients; set => m_otherIngredients = value; }
    public float _currentHeight { get => currentHeight; set => currentHeight = value; }

    protected virtual void Start()
    {
        m_otherIngredients.Add(this.gameObject);
    }
}
