using DG.Tweening;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class Utility : InputManager
{
    [Header("Variable\n")]
    [SerializeField] private float m_swipeMaxLenght = 0.8f;
    [SerializeField] private float m_rotationSpeed = 0.2f;

    [Header("Reference\n")]
    [SerializeField] private Transform pointPos;
    [SerializeField] protected MapGeneration map;
    [SerializeField] Button btn_undo;
    [SerializeField] GameObject PNL_UIGame;

    #region PRIVATE VARIABLE
    protected Vector3 m_startPos, m_endPos;
    protected RaycastHit m_hit;

    protected GameObject m_currentMapPos;
    protected GameObject m_targetMapPos;
    protected List<GameObject> m_currentMapList;
    protected List<GameObject> m_targetMapList;

    protected bool m_isRotate = true;
    protected bool m_isUndo = false;
    #endregion

    #region TOUCH METHOD
    protected Vector3 GetDirection(Vector3 start, Vector3 end)
    {
        var dirX = end.x - start.x;
        var dirY = end.y - start.y;

        if (Mathf.Abs(dirX) > Mathf.Abs(dirY))
            return dirX > 0 ? Vector3.right : Vector3.left;
        else
            return dirY > 0 ? Vector3.forward : Vector3.back;
    }
    protected bool Swipe(Vector3 startSwipe, Vector3 endSwipe)
    {
        var tmp = (startSwipe - endSwipe).magnitude;

        if (tmp > m_swipeMaxLenght)
        {
            return true;
        }
        return false;
    }
    #endregion

    #region CONTROL MAP
    protected void CreateParent(Transform hit, List<GameObject> myParent)
    {
        foreach (var item in myParent)
        {
            item.transform.parent = hit.transform;
        }
    }
    protected void DestroyParent(List<GameObject> myParent)
    {
        foreach (var item in myParent)
        {
            item.transform.parent = map.transform;
        }
    }
    protected void UpdatePosition(MapGeneration map)
    {
        for (int i = 0; i < map._prefabsMap.GetLength(0); i++)
        {
            for (int j = 0; j < map._prefabsMap.GetLength(1); j++)
            {
                if (map._prefabsMap[i, j] == null) continue;

                var tmp = map._prefabsMap[i, j].GetComponent<myIngredient>();
                tmp._currentHeight = tmp.transform.GetChild(0).localScale.y * (tmp._otherIngredients.Count - 1);
            }
        }
    }
    protected void CreateSandwich(Transform currentHit, Vector3 hitPosition, GameObject targetObj)
    {
        if (!m_isUndo)
            currentHit.transform.position = new Vector3(currentHit.transform.position.x, targetObj.GetComponent<myIngredient>()._currentHeight, currentHit.transform.position.z);
        else
            currentHit.transform.position = new Vector3(currentHit.transform.position.x, hitPosition.y, currentHit.transform.position.z);
    }
    protected void CreateList(Transform hit)
    {
        for (int i = 0; i < m_currentMapList.Count; i++)
        {
            if (!m_targetMapList.Contains(m_currentMapList[i]))
            {
                m_targetMapList.Add(m_currentMapList[i]);
            }
        }
        m_targetMapPos.GetComponent<myIngredient>()._currentHeight += hit.transform.GetChild(0).localScale.y;
        map._prefabsMap[(int)hit.position.x, (int)hit.position.z] = null;
    }
    protected bool CheckCollision(Transform hit)
    {
        // C1

        if (m_targetMapPos == null) return false;
        if (hit.transform.position.y != m_currentMapPos.GetComponent<myIngredient>()._currentHeight) return false;

        // C2
        if (m_currentMapPos.GetComponent<IBread>() != null && m_targetMapPos.GetComponent<IBread>() != null)
        {
            if (m_currentMapList.Count + m_targetMapList.Count == map._allIngredients.Count)
                return true;
        }

        //3
        if (m_currentMapPos.GetComponent<IBread>() != null)
        {
            if (m_currentMapList.Count < map._allIngredients.Count - 1) return false;
        }

        //Default
        return true;
    }
    private void CheckEndGame()
    {
        if (GM._objInGame == 1)
        {
            PNL_UIGame.SetActive(true);
        }
    }
    #endregion

    #region ROTATION
    protected void RotateObj(Transform hit, Vector3 dir, float rotX, float rotZ, Vector3 hitPosition, List<GameObject> myParent)
    {
        if (!m_isUndo)
            hit.transform.position = new Vector3(hit.position.x, m_targetMapPos.GetComponent<myIngredient>()._currentHeight, hit.position.z);
        var point = Instantiate(pointPos.gameObject, hit.transform.position + new Vector3(dir.x * (hit.transform.localScale.x / 2), 0, dir.z * (hit.transform.localScale.z / 2)), Quaternion.identity);
        hit.SetParent(point.transform, true);

        Vector3 rotation = new Vector3(dir.z * rotX, 0, dir.x * rotZ);

        point.transform.DORotate(rotation, m_rotationSpeed).OnComplete(() => CompleteAction(hit.transform, point.transform, hitPosition, myParent));

    }
    private void CompleteAction(Transform hit, Transform point, Vector3 hitPosition, List<GameObject> myParent)
    {
        CreateSandwich(hit, hitPosition, m_targetMapPos);
        DestroyParent(myParent);
        Destroy(point.gameObject);
        ResetPositionSandwich(myParent);
        CheckEndGame();

        btn_undo.enabled = true;
        m_isRotate = true;
        m_isUndo = false;
    }
    protected void ResetPositionSandwich(List<GameObject> targetList)
    {
        foreach (var item in targetList)
        {
            decimal tmp = (decimal)item.transform.position.y;
            item.transform.position = new Vector3(Mathf.RoundToInt(item.transform.position.x), (float)Math.Round(tmp, 1, MidpointRounding.AwayFromZero), Mathf.RoundToInt(item.transform.position.z));
        }
        ResetVariable();
    }
    #endregion
    public void ResetVariable()
    {
        m_hit = new RaycastHit();
        m_startPos = Vector3.zero;
        m_endPos = Vector3.zero;
        m_currentMapPos = null;
        m_targetMapPos = null;
        m_currentMapList = null;
        m_targetMapList = null;

    }

    #region UNDO
    public void UNDO()
    {
        if (GM._games.Count > 0 && !m_isUndo && m_isRotate)
        {
            m_isUndo = true;
            GM._objInGame++;

            var tmp = GM._games[GM._currentMove - 1];

            UpdatePosition(map);
            CreateParent(tmp.hit, tmp.parentList);
            RotateObj(tmp.hit, -tmp.direction, 180, -180, tmp.position, tmp.parentList);
            ResetUndo(tmp, map);

            GM._games.RemoveAt(GM._currentMove - 1);
            GM._currentMove--;

            btn_undo.enabled = false;
        }
    }
    private void ResetUndo(VarGame variable, MapGeneration map)
    {
        map._prefabsMap[(int)variable.position.x, (int)variable.position.z] = variable.currentBase.gameObject;
        var resetBase = map._prefabsMap[(int)(variable.position.x + variable.direction.x), (int)(variable.position.z + variable.direction.z)].GetComponent<myIngredient>();

        foreach (var item in variable.parentList)
        {
            if (resetBase._otherIngredients.Contains(item))
            {
                resetBase._otherIngredients.Remove(item);
            }
        }
    }
    #endregion
}