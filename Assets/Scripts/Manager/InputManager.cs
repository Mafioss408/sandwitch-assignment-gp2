using DG.Tweening;
using UnityEngine;


public abstract class InputManager : MonoBehaviour
{

    protected Camera camera;
    protected GameManager GM;


    protected void Awake()
    {
        camera = Camera.main;
        DOTween.Init(true, true);
    }

    protected void Start()
    {
        GM = GameManager.Instance;
    }

    protected virtual void Update()
    {

        if (Input.touchCount == 0) return;

        Touch touch = Input.GetTouch(0);

        switch (touch.phase)
        {
            case TouchPhase.Began:
                TouchBegan(touch);
                break;
            case TouchPhase.Moved:
                TouchMoved(touch);
                break;
            case TouchPhase.Stationary:
                break;
            case TouchPhase.Ended:
                TouchEnded(touch);
                break;
            case TouchPhase.Canceled:
                break;
            default:
                break;
        }
    }


    protected virtual void TouchBegan(Touch touch) { }

    protected virtual void TouchMoved(Touch touch) { }

    protected virtual void TouchEnded(Touch touch) { }


}

