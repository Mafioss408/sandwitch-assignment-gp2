using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class VarGame
{
    public Transform hit;
    public List<GameObject> parentList;
    public Vector3 direction;
    public Vector3 position;
    public Transform currentBase;

    public VarGame(Transform hit, List<GameObject> parent, Vector3 direction, Vector3 position, Transform currentBase)
    {
        this.hit = hit;
        this.parentList = parent;
        this.direction = direction;
        this.position = position;
        this.currentBase = currentBase;
    }
}

public class GameManager : MonoBehaviour
{

    [SerializeField] MapGeneration map;

    private int currentLvl = 0;
    private int m_currentMove;
    private int m_objInGame;
    private List<VarGame> m_games = new List<VarGame>();


    #region GET/SET
    public int _currentLvl { get => currentLvl; set => currentLvl = value; }
    public int _currentMove { get => m_currentMove; set => m_currentMove = value; }
    public int _objInGame { get => m_objInGame; set => m_objInGame = value; }
    public List<VarGame> _games { get => m_games; set => m_games = value; }
    #endregion

    private static GameManager instance;
    public static GameManager Instance { get => instance; set => instance = value; }

    private void Awake()
    {
        Instance = this;
    }

    public void NextLevel()
    {
        if (currentLvl == map._levels.Count - 1)
            _currentLvl = 0;
        else
            currentLvl++;
    }

}
