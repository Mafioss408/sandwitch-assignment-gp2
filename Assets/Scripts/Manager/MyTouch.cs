using System;
using UnityEngine;

public class MyTouch : Utility
{
    protected override void TouchBegan(Touch touch)
    {
        base.TouchBegan(touch);

        if (m_isRotate && !m_isUndo)
        {
            m_startPos = camera.ScreenToWorldPoint(touch.position);
            m_endPos = camera.ScreenToWorldPoint(touch.position);

            Ray ray = camera.ScreenPointToRay(new Vector3(touch.position.x, touch.position.y, 0f));
            Physics.Raycast(ray, out m_hit, Mathf.Infinity);

            if (m_hit.transform == null) return;

            m_currentMapPos = map._prefabsMap[(int)m_hit.transform.position.x, (int)m_hit.transform.position.z];
            m_currentMapList = m_currentMapPos.GetComponent<myIngredient>()._otherIngredients;

            CreateParent(m_hit.transform, m_currentMapList);
            UpdatePosition(map);
        }
    }
    protected override void TouchMoved(Touch touch)
    {
        base.TouchMoved(touch);
        if (m_isRotate && !m_isUndo)
            m_endPos = camera.ScreenToWorldPoint(touch.position);

    }
    protected override void TouchEnded(Touch touch)
    {
        base.TouchEnded(touch);

        if (m_isRotate && !m_isUndo)
        {
            var dir = GetDirection(m_startPos, m_endPos);

            try
            {
                m_targetMapPos = map._prefabsMap[(int)(m_hit.transform.position.x + dir.x), (int)(m_hit.transform.position.z + dir.z)];
                m_targetMapList = m_targetMapPos.GetComponent<myIngredient>()._otherIngredients;
            }
            catch (NullReferenceException) { }
            catch (IndexOutOfRangeException)
            {
                ResetPositionSandwich(map._allIngredients);

                foreach (var item in map._allIngredients)
                {
                    item.transform.parent = map.transform;
                }

                ResetVariable();
            }

            if (Swipe(m_startPos, m_endPos) && CheckCollision(m_hit.transform))
            {
                m_isRotate = false;
                GM._objInGame--;
                GM._currentMove++;


                GM._games.Add(new VarGame(m_hit.transform, m_currentMapList, dir, m_hit.transform.position, m_currentMapPos.transform));

                CreateList(m_hit.transform);
                RotateObj(m_hit.transform, dir, 180, -180, m_hit.transform.position, m_targetMapList);
            }
            else if (!m_isUndo && m_hit.collider != null)
            {
                DestroyParent(m_currentMapList);
                ResetVariable();
                m_isRotate = true;
            }
        }
    }
}
